import React,{Component } from 'react'
import Login from './login'
import Welcome from './pages/welcome';
import Register from './register';
import { ToastContainer} from 'react-toastify';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch,
    useParams
} from "react-router-dom";
import 'react-toastify/dist/ReactToastify.css';
import { Container, Row, Col } from 'react-grid-system';
class MainView extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount(){


    }

    //manejador de login, debe pasarse al componente hijo
    handleLogin =(status)=>{

        if(status){
            this.props.actions.login(true);

        }else{
            this.props.actions.login(false);
        }
    }




    render () {


        //define el background del login
        const backgroundImageStyle = {
            backgroundImage: 'url(https://image.freepik.com/free-vector/spot-light-background_1284-4685.jpg)',
            height: '100%',
            backgroundPosition : 'center',
            backgroundSize : 'cover',
            backgroundRepeat : 'no-repeat'
        };



        if(this.props.state.IS_LOGED){
            return(
                <div>
                    <ToastContainer/>
                    <Welcome handleLogin={this.handleLogin}/>
                </div>

            )
        }else{
            return (
                <div>
                    <ToastContainer/>
                    <div style={backgroundImageStyle}>
                        <Container>
                            <Row style={{justifyContent: 'center'}}>
                                <Col  xs={12} md={6} lg={6}>
                                    <div className='login-container'>
                                        <Router>
                                            <div>
                                                <Switch>
                                                    <Route path="/createuser">
                                                        <Register />
                                                    </Route>
                                                    <Route path="/">
                                                        <Login handleLogin={this.handleLogin}/>
                                                    </Route>
                                                </Switch>
                                            </div>
                                        </Router>

                                    </div>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                </div>
            )
        }
    }
}




export default MainView