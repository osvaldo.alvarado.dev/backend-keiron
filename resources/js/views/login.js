import React,{Component } from "react";
import { Container, Row, Col } from 'react-grid-system';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import {postData,okAlert,errorAlert} from '../helpers/helpers';
import GLOBALS from '../helpers/globals';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch,
    useParams
} from "react-router-dom";


class Login extends Component {

    constructor(props) {
        super(props),
            this.state = {
                user: '',
                password: '',

            }
    }

    //onchange generico!, actualiza el STATE de igual nombre al input
    onChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    sendLogin = () => {
        const email = this.state.user;
        const password = this.state.password;
        const data = {email,password}

        postData(GLOBALS.SERVER_URL + '/api/auth/login',data).then(res=>{
            if(res.status == 200){

                okAlert("Inicio de sesión correcto!");
                GLOBALS.TOKEN = "Bearer " + res.data.access_token;
                GLOBALS.USERTYPE_ID = res.data.usertype_id;
                GLOBALS.USER_NAME = res.data.user_name;
                this.props.handleLogin(true);
            }else{
                errorAlert("Inicio de sesión incorrecto!");
                this.props.handleLogin(false);
            }
        });


    }



    render() {

        return (

                                <Card >
                                    <CardContent >
                                        <Typography variant="headline" component="h2"  color="textSecondary">
                                            Aplicacion de Pruebas
                                        </Typography>
                                        <br />
                                        <div >
                                            <FormControl >
                                                <InputLabel htmlFor="input-with-icon-adornment">Email:</InputLabel>

                                                <Input
                                                    name='user'
                                                    value={this.state.user}
                                                    onChange={this.onChange}
                                                    id="input-with-icon-adornment"
                                                    startAdornment={
                                                        <InputAdornment position="start">

                                                        </InputAdornment>
                                                    }
                                                />
                                            </FormControl>
                                        </div>
                                        <div>
                                            <FormControl>
                                                <InputLabel htmlFor="input-with-icon-adornment">Clave:</InputLabel>
                                                <Input
                                                    name="password"
                                                    type="password"
                                                    fullWidth={true}
                                                    id="input-with-icon-adornment"
                                                    onChange={this.onChange}
                                                    startAdornment={
                                                        <InputAdornment position="start">

                                                        </InputAdornment>
                                                    }
                                                />
                                            </FormControl>
                                        </div>


                                    </CardContent>
                                    <CardActions style={{justifyContent: 'center'}}>

                                        <Button onClick={() => (this.sendLogin())}  variant="contained" color="primary">
                                            Iniciar Sesion
                                        </Button>


                                        <Link to="/createuser">
                                            <Button variant="contained" color="secondary">
                                               Registrar
                                            </Button>
                                        </Link>
                                    </CardActions>
                                </Card>

        );
    }
}

export default Login

