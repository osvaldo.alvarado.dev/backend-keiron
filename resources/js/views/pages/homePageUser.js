import React,{Component } from "react";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import TicketsTable from '../../components/ticketsTable';
import GLOBALS from '../../helpers/globals';
class HomePageUser extends Component {

    constructor(props) {
        super(props),
            this.state = {

            }
    }

    render() {
        return (
            <Card >
                <CardContent>
                    <h3 className="header-title">Bienvenido usuario <b>{GLOBALS.USER_NAME}</b>    - Pantalla de Usuario Normal</h3>
                    <Divider />
                    <div>
                        <TicketsTable/>

                    </div>
                </CardContent>
            </Card>
        );
    }
}

export default HomePageUser

