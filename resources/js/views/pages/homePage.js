import React,{Component } from "react";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';


class HomePage extends Component {

    constructor(props) {
        super(props),
            this.state = {

            }
    }

    render() {
        return (
            <Card >
                <CardContent>
                    <h3 className="header-title">Programado por:</h3>
                    <Divider/>
                    <div>
                        <p>Osvaldo Alvarado</p>
                        <p><a href="https://www.osvaldoalvarado.com">https://www.osvaldoalvarado.com</a></p>
                        <p><a href="tel:+56973155545">+56973155545</a></p>
                        <p><a href="mailto:osvaldo.alvarado.dev@gmail.com">osvaldo.alvarado.dev@gmail.com</a></p>
                    </div>
                </CardContent>
            </Card>
        );
    }
}

export default HomePage

