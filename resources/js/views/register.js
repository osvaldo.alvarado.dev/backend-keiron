import React,{Component } from 'react'
import Login from './login'
import Welcome from './pages/welcome';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import {postData,okAlert,errorAlert} from '../helpers/helpers';
import GLOBALS from '../helpers/globals';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
class Register extends Component {
    constructor(props) {
        super(props),
            this.state = {
                mail: '',
                name : '',
                password: '',
                password_confirmation: '',
                usertype_id: 0
            }

    }

    //onchange generico!, actualiza el STATE de igual nombre al input
    onChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    sendRegister = () => {

        var email = this.state.email;
        var name = this.state.name;
        var password = this.state.password;
        var password_confirmation = this.state.password_confirmation;
        var usertype_id = this.state.usertype_id;


        const data = {email,name,password,password_confirmation,usertype_id}

        postData(GLOBALS.SERVER_URL + '/api/auth/signup',data).then(res=>{



            if(res.status == 201){
                okAlert("Se creo el usuario correctamente");
            }else{
                errorAlert("Error al crear el usuario");

            }
        });


    }
    render () {
      return (
          <Card >
              <CardContent >
                  <Typography variant="headline" component="h2"  color="textSecondary">
                      Crear un usuario nuevo
                  </Typography>
                  <br />
                  <div >
                      <FormControl >
                          <InputLabel htmlFor="input-with-icon-adornment">Email:</InputLabel>

                          <Input
                              name='email'
                              value={this.state.email}
                              onChange={this.onChange}
                              id="input-with-icon-adornment"
                          />
                      </FormControl>
                  </div>
                  <div >
                      <FormControl >
                          <InputLabel htmlFor="input-with-icon-adornment">Nombre:</InputLabel>

                          <Input
                              name='name'
                              value={this.state.name}
                              onChange={this.onChange}
                              id="input-with-icon-adornment"
                          />
                      </FormControl>
                  </div>


                  <div>
                      <FormControl>
                          <InputLabel htmlFor="input-with-icon-adornment">Clave:</InputLabel>
                          <Input
                              name="password"
                              type="password"

                              id="input-with-icon-adornment"
                              onChange={this.onChange}
                          />
                      </FormControl>
                  </div>

                  <div>
                      <FormControl>
                          <InputLabel htmlFor="input-with-icon-adornment">Confirmar Clave:</InputLabel>
                          <Input
                              name="password_confirmation"
                              type="password"

                              id="input-with-icon-adornment"
                              onChange={this.onChange}
                          />
                      </FormControl>
                  </div>
                  <div >
                      <FormControl >
                          <InputLabel htmlFor="input-with-icon-adornment">Tipo de Usuario:</InputLabel>
                          <Select
                              value={this.state.usertype_id}
                              name={'usertype_id'}
                              onChange={this.onChange}
                              style={{width:"100%"}}
                          >
                              <MenuItem value="1">Administrador</MenuItem>
                              <MenuItem value="2">Usuario Normal</MenuItem>
                          </Select>
                      </FormControl>
                  </div>

              </CardContent>
              <CardActions style={{justifyContent: 'center'}}>

                  <Button onClick={() => (this.sendRegister())}  variant="contained" color="secondary">
                      Crear Nuevo Usuario
                  </Button>


              </CardActions>
          </Card>
      )
    }
}




export default Register