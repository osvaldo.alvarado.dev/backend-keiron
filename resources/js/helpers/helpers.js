import axios from 'axios';
import GLOBALS from './globals';
import {  toast } from 'react-toastify';

export function getData(Url,Token){

    return axios.get(Url,{
      headers: {
        'Authorization': 'Bearer ' + GLOBALS.TOKEN
       },
       
    })
    .then(function (response) {
      console.log(response);
      return response;
    })
    .catch(function (error) {
      //console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
      if (error.response) {
      }
      return error;
    });
  }

  export function postData(Url,Item){

    return axios.post(Url,Item,{
    //return axios.post( Url,Item,{
      headers: {
        'Authorization': 'Bearer ' + GLOBALS.TOKEN,
       },
       timeout: 3000,
    })
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
        console.log("Error");
      console.log(error);
      return error.response;
    });
  }

export function deleteData(Url,Item){

    return axios.delete(Url,Item,{
        //return axios.post( Url,Item,{
        headers: {
            'Authorization': 'Bearer ' + GLOBALS.TOKEN
        },
        timeout: 3000,
    })
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            console.log("Error");
            console.log(error);
            return error.response;
        });
}


export function okAlert(text){
    toast.success(text);
}

export function errorAlert(text){
    toast.error(text);
}

