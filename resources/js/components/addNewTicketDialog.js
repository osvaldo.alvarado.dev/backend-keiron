import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Select from '@material-ui/core/Select';
import DialogTitle from '@material-ui/core/DialogTitle';
import {getData,deleteElement, okAlert, errorAlert,postData} from '../helpers/helpers';
import GLOBALS from '../helpers/globals';
import MenuItem from '@material-ui/core/MenuItem';
class AddNewTicketDialog extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            open: false,
            UserName: null,
            Email: null,
            user_id:null,
            user_list : []
        };
    }


    handleClickOpen = () => {
        this.setState({ open: true });
    };



    handleClose = () => {
        this.setState({ open: false });
    };

    createTicket=()=>{
        const user_id = this.state.user_id;
        const data = {user_id};
        postData(GLOBALS.SERVER_URL + '/api/tickets',data).then(res=>{
            if(res){
                if(res.status == 201){
                    okAlert("Se creo el Ticket Correctamente");
                    this.props.updateTable();
                    this.handleClose();
                }else{
                    errorAlert("No se pudo crear el Ticket");
                }
            }else{
                errorAlert("No se pudo guardar el Ticket :(");
            }
        });

    }

    //onchange generico!, actualiza el STATE de igual nombre al input
    onChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    //carga la lista de procesos
    componentWillMount(){
        getData(GLOBALS.SERVER_URL + '/api/userlist').then(res => {
            if(res.status == 200){
                this.setState({user_list : res.data})
            }
        }).catch(response =>{
            console.log("Error de respuesta: ")

        })
    }



    render() {
        return (
            <div>

                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Creando Nuevo Ticket"}</DialogTitle>
                    <DialogContent>
                        <b>Usuario :</b><br />
                        <Select
                            value={this.state.user_id}
                            name={'user_id'}
                            onChange={this.onChange}
                            style={{width:"100%"}}
                        >
                            {this.state.user_list.map(row => {
                                return (
                                    <MenuItem value={row.id}>{row.name}</MenuItem>
                                )})
                            }
                        </Select>

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancelar
                        </Button>
                        <Button onClick={this.createTicket} color="primary" autoFocus>
                            Crear Ticket
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default AddNewTicketDialog;