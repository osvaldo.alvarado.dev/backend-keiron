import React,{Component } from 'react'
import GLOBALS from '../helpers/globals';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import {getData, deleteElement, okAlert, errorAlert, postData, deleteData} from '../helpers/helpers';
import AddNewTicketDialog from './addNewTicketDialog';
import Check from '@material-ui/icons/Check';
import Close from '@material-ui/icons/Close';

class TicketsTable extends Component {
    constructor(props) {
        super(props),
            this.state = {
                ticketsList: null,
                userSelected: null
            }

        this.addTicketChildDialog = React.createRef();
    }



    componentWillMount(){
        this.updateTable();
    }

    updateTable(){
        getData(GLOBALS.SERVER_URL+ "/api/tickets/").then(res => {

            this.setState({ticketsList : res.data})
        }).catch(response =>{
            console.log(response);
        })

    }

    addNewTicketDialog(){
        this.addTicketChildDialog.current.handleClickOpen();
    }


    takeTicket(ticket_id){
        postData(GLOBALS.SERVER_URL+ "/api/tickets/" + ticket_id + "/taketicket").then(res => {
            if(res.status == 200){
                okAlert("Ticket Tomado " + ticket_id);
            }else{
                errorAlert("No se pudo tomar el ticket" + ticket_id)
            }
        }).catch(response =>{
            errorAlert("No se pudo tomar el ticket" + ticket_id)
            console.log("Error de respuesta: ")
            console.log(response);
        })
        this.updateTable();
    }


    deleteTicket(ticket_id){
        deleteData(GLOBALS.SERVER_URL+ "/api/tickets/" + ticket_id ).then(res => {
            if(res.status == 200){
                okAlert("Ticket Eliminado " + ticket_id);
            }else{
                errorAlert("No se pudo eliminar el ticket" + ticket_id)
            }
        }).catch(response =>{
            errorAlert("No se pudo eliminar el ticket" + ticket_id)
            console.log("Error de respuesta: ")
            console.log(response);
        })
        this.updateTable();

    }




    render () {
        if(this.state.ticketsList){
            return (
                <div>

                    <AddNewTicketDialog ref={this.addTicketChildDialog} updateTable={()=>{this.updateTable()}}/>
                    {GLOBALS.USERTYPE_ID == 1
                        ? <Button color="primary" variant="contained" onClick={()=>{this.addNewTicketDialog()}} > + Crear Nuevo Ticket</Button>
                        : <h1></h1>
                    }
                    <Paper>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Ticket ID</TableCell>
                                    <TableCell>Fecha Creacion</TableCell>
                                    <TableCell>Ticket Tomado</TableCell>
                                    <TableCell>Usuario Asignado</TableCell>
                                    <TableCell>Opciones</TableCell>

                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.ticketsList.map(row => {
                                    return (
                                        <TableRow key={row.id}>

                                            <TableCell>{row.id}</TableCell>
                                            <TableCell>{row.created_at}</TableCell>
                                            <TableCell>
                                                {row.ticket_order== 0
                                                    ? <div><Close color="secondary" /> Pendiente</div>
                                                    : <div><Check color="primary"/> Tomado</div>
                                                }
                                            </TableCell>
                                            <TableCell component="th" scope="row">
                                                {row.user_name}
                                            </TableCell>
                                            <TableCell component="th" scope="row">
                                                {GLOBALS.USERTYPE_ID == 2
                                                    ? <Button color="primary" variant="contained" onClick={()=>{this.takeTicket(row.id)}} > + Tomar Ticket</Button>
                                                    : <div>
                                                        <Button color="secondary" variant="contained" onClick={()=>{this.deleteTicket(row.id)}} > x Eliminar Ticket</Button>
                                                        </div>
                                                }
                                            </TableCell>


                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>


            )
        }else{
            return(
                <div>
                    {GLOBALS.USERTYPE_ID == 1
                        ? <Button color="primary" variant="contained" onClick={()=>{this.addNewTicketDialog()}} > + Crear Nuevo Ticket</Button>
                        : <h1></h1>
                    }
                    <h3>No Existen Tickets Aun</h3>
                </div>
            )
        }
    }
}

export default TicketsTable