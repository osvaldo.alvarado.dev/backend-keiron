<?php

namespace App\Http\Controllers;

use App\Tickets;
use Illuminate\Http\Request;

class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Tickets::select('tickets.*','users.name as user_name','users.email as user_email')
            ->leftJoin('users','users.id','tickets.user_id')
            ->orderBy('id','desc')
            ->get();

        return $tickets;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ticket = New Tickets();
        $input = $request->all();

        //valida los parametros de entrada
        if(isset($input['user_id'])){
            $ticket->user_id = $input['user_id'];

            if($ticket->save()){
                return $ticket;
            }else{
                return response()->json([
                    'error' => 'No se pudo crear el ticket'], 401);
            }
        }else{
            return response()->json([
                'error' => 'Faltan parametros para poder crear el ticket'], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tickets  $tickets
     * @return \Illuminate\Http\Response
     */
    public function show($ticket_id)
    {
        $ticket = Tickets::findOrFail($ticket_id);
        return $ticket;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tickets  $tickets
     * @return \Illuminate\Http\Response
     */
    public function edit(Tickets $tickets)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tickets  $tickets
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tickets $tickets)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tickets  $tickets
     * @return \Illuminate\Http\Response
     */
    public function destroy($ticket_id)
    {
        $ticket = Tickets::findOrFail($ticket_id);
        $ticket->delete();


        return response()->json([
            'ok' => 'Todo ejecutado correctamente'], 200);
    }


    public function takeTicket($ticket_id){
        $ticket = Tickets::findOrFail($ticket_id);
        $ticket->ticket_order = 1;
        $ticket->save();
        return($ticket);

    }
}
