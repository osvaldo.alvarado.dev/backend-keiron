<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testListadoUsuarios()
    {
        $response = $this->get('api/userlist');
        $response->assertStatus(200);
    }


    public function testloginUsuario()
    {
        $data = ['email'=>'osvaldo.alvarado.dev@gmail.com','password'=>'password','remember_me'=>"1"];
        $response = $this->json("post",'api/auth/login',$data);
        $response->assertStatus(200);
    }

    public function testRegistrarUsuario(){
        $length = 10;
        //genera un mail aleatorio con 10 caracteres de largo
        $email =  substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,$length)."@gmail.com";
        $data = ['name'=>'Usuario Automatico','email'=>$email,'password'=>'password','password_confirmation'=>'password','usertype_id'=>1];
        $response = $this->json("post",'api/auth/signup',$data);
        $response->assertStatus(201);

    }
}
