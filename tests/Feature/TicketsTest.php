<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TicketsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCrearTickets()
    {
        $data = ['user_id'=>"1"];
        $response = $this->json("post",'/api/tickets',$data);
        $response->assertStatus(201);
    }

    public function testGetTickets()
    {
        $response = $this->get('api/tickets');
        $response->assertStatus(200);
    }

    public function testTakeTickets(){
        $response = $this->json("post",'/api/tickets/1/taketicket');
        $response->assertStatus(200);
    }
}
