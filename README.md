#Proyecto realizado para Keiron
## Como ejecutar este proyecto
Ejecutar los siguientes comandos:
- git clone https://gitlab.com/osvaldo.alvarado.dev/backend-keiron.git
- composer install
- npm install
- cp .env.example .env
- (insertar datos de BD en el archivo .env para poder ejecutar los siguientes pasos)
- php artisan migrate:fresh --seed
- (despues de ejecutar el siguiente paso utilizar cualquier palabra clave para generar la key)
- php artisan passport:client --personal
- php artisan serve

## Test añadidos

Para este proyecto ademas se añadieron test automatizados que se pueden ejecutar con el comando:
- php artisan test

Estos testean los siguientes end-point:
- Crear Tickets
- Obtener Tickets
- Tomar los Tickets
- Obtener Usuarios
- Crear Usuarios
- Iniciar Sesion

## Mis datos de Contacto:

- **[Mi web Personal](https://www.osvaldoalvarado.com/)**
- **[osvaldo.alvarado.dev@gmail.com](mailto:osvaldo.alvarado.dev@gmail.com)**