<?php

use Illuminate\Database\Seeder;
use App\UserTypes;

class UserTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userType = New UserTypes();
        $userType->name = "Administrador";
        $userType->save();

        $userType = New UserTypes();
        $userType->name = "Usuario";
        $userType->save();
    }
}
