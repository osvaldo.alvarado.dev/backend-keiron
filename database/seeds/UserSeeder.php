<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Osvaldo Alvarado';
        $user->email = 'osvaldo.alvarado.dev@gmail.com';
        $user->password = bcrypt('password');
        $user->usertype_id = 1;
        $user->save();

        $user = new User();
        $user->name = 'Usuario Normal';
        $user->email = 'usuarionormal@gmail.com';
        $user->password = bcrypt('password');
        $user->usertype_id = 2;
        $user->save();
    }
}
